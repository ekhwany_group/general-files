/*
 * LED_interface.h
 *
 *  Created on: Feb 5, 2019
 *      Author: EsSss
 */


/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* TARGET: LED-Interface File							*/
/* DATE: 31 JAN 2019									*/
/* VERSION : V1.0										*/
/********************************************************/
/* DESCRIPTION: 										*/
/*  This file is used to Interface the Led Component	*/
/********************************************************/


#ifndef LED_INTERFACE_H_
#define LED_INTERFACE_H_



#define LED_u8_INIT		(u8)0

/********************************************************/
/* DESCRIPTION: 										*/
/*  This file is used to Initialize the Led Component	*/
/********************************************************/
void LED_VidLedInit(void);

/********************************************************/
/* DESCRIPTION: 										*/
/*  This file is used Configure Any LED ON				*/
/********************************************************/
u8 LED_u8LedOn(u8 Copy_u8_LEDNb);

/********************************************************/
/* DESCRIPTION: 										*/
/*  This file is used Configure Any LED OFF				*/
/********************************************************/
u8 LED_u8LedOff(u8 Copy_u8_LEDNb);





#endif /* LED_INTERFACE_H_ */
