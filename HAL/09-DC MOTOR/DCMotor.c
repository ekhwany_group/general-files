/*
 * DcMotor.c
 *
 *  Created on: Mar 13, 2019
 *      Author: EsSss
 */


/****************************************************/
/* Author : Islam Gamal Abd El-Naser				*/
/* Date   : 13 Mar 2019								*/
/* Driver : DC Motor Driver 						*/
/* Layer  : HAL 									*/
/* Description : Motor Driver That Implemented to 	*/
/* 				Control Some feature related to DC  */
/* 				Motors such as switch the motor on  */
/* 				and off in specific direction 		*/
/****************************************************/

/* LIBs Layer */
#include "STD_TYPES.h"
#include "BIT_CALC.h"
/* MCAL Layer */
#include "RCC_interface.h"
#include "DIO_Interface.h"
/* HAL Layer  */
#include "DCMotor.h"



/* Global Variable Definition */
u8 DCMotor_LastState = DCMotor_CW;

 /* API : void DCMotor_VidInit								*/
 /* Description: Initialize The DC MOTOR as Switched On 	*/
 /* 			 In The ClockWise Direction and save the    */
 /* 			 last Running Direction to Start later with */
 /***********************************************************/
 /* Inputs  : Motor Index 									*/
 /* Outputs : No OutPuts									*/
 /***********************************************************/
void DCMotor_VidInit(u8 Copy_u8MotrIndex){
	/* Choose Specific Motor To Enable */
	switch(Copy_u8MotrIndex){
		case DCMOTOR_INDEX1:	Motor_Enable(DCMOTOR_INDEX1, (Register)DCMotor_CW); break;
		case DCMOTOR_INDEX2:	Motor_Enable(DCMOTOR_INDEX2, (Register)DCMotor_CW); break;
		case DCMOTOR_INDEX3:	Motor_Enable(DCMOTOR_INDEX3, (Register)DCMotor_CW); break;
		default			   :    /* Do Nothing */ 			  break;
	}
}

/* API : DCMotor_ClockWise								     */
/* Description: Switched The Motor On In ClockWise Direction */
/*************************************************************/
/* Inputs  : Motor Index									 */
/* Outputs : Functionality Validation 		   				 */
/*************************************************************/
u8 DCMotor_ClockWise(u8 Copy_u8MotrIndex){

	/* Locale Variable Definition */
	u8 Locale_ErorState = ERROR_OK;
	/* Choose Specific Motor To Enable */
	switch(Copy_u8MotrIndex){
		case DCMOTOR_INDEX1:	Locale_ErorState = Motor_Enable(DCMOTOR_INDEX1,(Register)DCMotor_CW); break;
		case DCMOTOR_INDEX2:	Locale_ErorState = Motor_Enable(DCMOTOR_INDEX2,(Register)DCMotor_CW); break;
		case DCMOTOR_INDEX3:	Locale_ErorState = Motor_Enable(DCMOTOR_INDEX3,(Register)DCMotor_CW); break;
		default			   :    Locale_ErorState = ERROR_NOK;
	}
	/* Return Error State */
	return Locale_ErorState;
}

/* API : DCMotor_CounterClockWise   			          */
/* Description: Switched The Motor On In CounteClockWise  */
/* 						 Direction.						  */
/**********************************************************/
/* Inputs  : Motor Index 								  */
/* Outputs : Functionality Validation 		   			  */
/**********************************************************/
u8 DCMotor_CounterClockWise(u8 Copy_u8MotrIndex){

	/* Locale Variable Definition */
	u8 Locale_ErorState = ERROR_OK;
	/* Choose Specific Motor To Enable */
	switch(Copy_u8MotrIndex){
		case DCMOTOR_INDEX1:	Locale_ErorState =Motor_Enable(DCMOTOR_INDEX1,(Register)DCMotor_CCW); break;
		case DCMOTOR_INDEX2:	Locale_ErorState =Motor_Enable(DCMOTOR_INDEX2,(Register)DCMotor_CCW); break;
		case DCMOTOR_INDEX3:	Locale_ErorState =Motor_Enable(DCMOTOR_INDEX3,(Register)DCMotor_CCW); break;
		default			   :    Locale_ErorState = ERROR_NOK;
	}
	/* Return Error State */
	return Locale_ErorState;
}


/* API : DCMotor_SwitchedOFF		   			          */
/* Description: Switched The Motor OFF.					  */
/**********************************************************/
/* Inputs  : Motor Index								  */
/* Outputs : Functionality Validation 		   			  */
/**********************************************************/
u8 DCMotor_SwitchedOFF(u8 Copy_u8MotrIndex){

	u8 Locale_ErrorState = ERROR_OK;
		/* Case Clock Wise Direction Activation */
		switch(Copy_u8MotrIndex){
			case DCMOTOR_INDEX1:	DIO_u8SetPinValue(DCMotor_INDEX1_TERM1,((Register)DCMotor_OFF).BitAccess.Bit0);
									DIO_u8SetPinValue(DCMotor_INDEX1_TERM2,((Register)DCMotor_OFF).BitAccess.Bit1);
									break;
			case DCMOTOR_INDEX2:	DIO_u8SetPinValue(DCMotor_INDEX2_TERM1,((Register)DCMotor_OFF).BitAccess.Bit0);
									DIO_u8SetPinValue(DCMotor_INDEX2_TERM2,((Register)DCMotor_OFF).BitAccess.Bit1);
									break;
			case DCMOTOR_INDEX3:	DIO_u8SetPinValue(DCMotor_INDEX3_TERM1,((Register)DCMotor_OFF).BitAccess.Bit0);
									DIO_u8SetPinValue(DCMotor_INDEX3_TERM2,((Register)DCMotor_OFF).BitAccess.Bit1);
									break;
			default			   :  Locale_ErrorState = ERROR_NOK;
		}
		/* Return Error Level State */
	return Locale_ErrorState;
}




/********************************************************************/
/********************************************************************/


/********************************************************************/
/* 					DC MOTOR Private Functions                 		*/
/********************************************************************/


/* Function : Motor_Enable								   */
/* Description: Private Function That Confirm The Motor    */
/* 			    Enable Process.							   */
/***********************************************************/
/* Inputs  : Motor Index 								   */
/* Outputs : Functionality Validation					   */
/***********************************************************/
static u8 Motor_Enable(u8 Copy_u8MotrIndex, Register Copy_u8MotorDir){
	/* Locale Variable Definition */
	u8 Locale_ErrorState = ERROR_OK;
	/* Case Clock Wise Direction Activation */

	if (Copy_u8MotorDir.ByteAccess == DCMotor_CW){
		switch(Copy_u8MotrIndex){
			case DCMOTOR_INDEX1:	DIO_u8SetPinValue(DCMotor_INDEX1_TERM1,Copy_u8MotorDir.BitAccess.Bit0);
									DIO_u8SetPinValue(DCMotor_INDEX1_TERM2,Copy_u8MotorDir.BitAccess.Bit1);
									break;
			case DCMOTOR_INDEX2:	DIO_u8SetPinValue(DCMotor_INDEX2_TERM1,Copy_u8MotorDir.BitAccess.Bit0);
									DIO_u8SetPinValue(DCMotor_INDEX2_TERM2,Copy_u8MotorDir.BitAccess.Bit1);
									break;
			case DCMOTOR_INDEX3:	DIO_u8SetPinValue(DCMotor_INDEX3_TERM1,Copy_u8MotorDir.BitAccess.Bit0);
									DIO_u8SetPinValue(DCMotor_INDEX3_TERM2,Copy_u8MotorDir.BitAccess.Bit1);
									break;
			default			   :  	Locale_ErrorState = ERROR_NOK;
		}
	}
	/* Case Counter Clock Wise Direction Activation */
	else if (Copy_u8MotorDir.ByteAccess == DCMotor_CCW){
		switch(Copy_u8MotrIndex){
			case DCMOTOR_INDEX1:	DIO_u8SetPinValue(DCMotor_INDEX1_TERM1,Copy_u8MotorDir.BitAccess.Bit0);
									DIO_u8SetPinValue(DCMotor_INDEX1_TERM2,Copy_u8MotorDir.BitAccess.Bit1);
									break;
			case DCMOTOR_INDEX2:	DIO_u8SetPinValue(DCMotor_INDEX2_TERM1,Copy_u8MotorDir.BitAccess.Bit0);
									DIO_u8SetPinValue(DCMotor_INDEX2_TERM2,Copy_u8MotorDir.BitAccess.Bit1);
									break;
			case DCMOTOR_INDEX3:	DIO_u8SetPinValue(DCMotor_INDEX3_TERM1,Copy_u8MotorDir.BitAccess.Bit0);
									DIO_u8SetPinValue(DCMotor_INDEX3_TERM2,Copy_u8MotorDir.BitAccess.Bit1);
									break;
			default			   :  Locale_ErrorState = ERROR_NOK;
		}
	}
	else{
		/* Not Valid Direction */
		Locale_ErrorState = ERROR_NOK;
	}
	/* Error Validation State : */
	return Locale_ErrorState;
}

