/*
 * DcMotor.h
 *
 *  Created on: Mar 13, 2019
 *      Author: EsSss
 */

#ifndef DCMOTOR_H_
#define DCMOTOR_H_



/****************************************************/
/* Author : Islam Gamal Abd El-Naser				*/
/* Date   : 13 Mar 2019								*/
/* Driver : DC Motor Driver 						*/
/* Layer  : HAL 									*/
/* Description : Motor Driver That Implemented to 	*/
/* 				Control Some feature related to DC  */
/* 				Motors such as switch the motor on  */
/* 				and off in specific direction 		*/
/****************************************************/


/***************************************/
/* 		DC MOTOR PreCONFIGURATIO	   */
/***************************************/

// MOTOR Indexing Guide
#define DCMOTOR_INDEX1			   (u8)0
#define DCMOTOR_INDEX2			   (u8)1
#define DCMOTOR_INDEX3			   (u8)2

// TERMINALS OF MOTORS :
#define DCMotor_INDEX1_TERM1	   DIO_PIN0
#define DCMotor_INDEX1_TERM2       DIO_PIN1
#define DCMotor_INDEX2_TERM1       DIO_PIN2
#define DCMotor_INDEX2_TERM2       DIO_PIN3
#define DCMotor_INDEX3_TERM1       DIO_PIN4
#define DCMotor_INDEX3_TERM2       DIO_PIN5

/**********************************************************************/
/*********************************************************************/

/***************************************/
/* 		DC MOTOR INTERFACES			   */
/***************************************/

// CLockWise & Anti ClockWise Direction:
#define DCMotor_CW		(u8)0b01
#define DCMotor_CCW		(u8)0b10

// Motor Switched OFF:
#define DCMotor_OFF		(u8)0b00
/**************************************/

/*********************************************************************/
/*********************************************************************/



/********************************************************************/
/* 					DC MOTOR APIs			                   		*/
/********************************************************************/

/* API : void DCMotor_VidInit							   */
/* Description: Initialize The DC MOTOR as Switched On 	   */
/* 			 In The ClockWise Direction and save the       */
/* 			 last Running Direction to Start later with    */
/***********************************************************/
/* Inputs  : Motor Index 								   */
/* Outputs : No OutPuts									   */
/***********************************************************/
void DCMotor_VidInit(u8 Copy_u8MotrIndex);
/* API : DCMotor_ClockWise								     */
/* Description: Switched The Motor On In ClockWise Direction */
/*************************************************************/
/* Inputs  : Motor Index									 */
/* Outputs : Functionality Validation 		   				 */
/*************************************************************/
u8 DCMotor_ClockWise(u8 Copy_u8MotrIndex);
/* API : DCMotor_CounterClockWise   			          */
/* Description: Switched The Motor On In CounteClockWise  */
/* 						 Direction.						  */
/**********************************************************/
/* Inputs  : Motor Index 								  */
/* Outputs : Functionality Validation 		   			  */
/**********************************************************/
u8 DCMotor_CounterClockWise(u8 Copy_u8MotrIndex);
/* API : DCMotor_SwitchedOFF		   			          */
/* Description: Switched The Motor OFF.					  */
/**********************************************************/
/* Inputs  : Motor Index								  */
/* Outputs : Functionality Validation 		   			  */
/**********************************************************/
u8 DCMotor_SwitchedOFF(u8 Copy_u8MotrIndex);


/********************************************************************/
/********************************************************************/



/********************************************************************/
/* 					DC MOTOR Private Functions                 		*/
/********************************************************************/


/* Function : Motor_Enable								   */
/* Description: Private Function That Confirm The Motor    */
/* 			    Enable Process.							   */
/***********************************************************/
/* Inputs  : Motor Index 								   */
/* Outputs : Functionality Validation					   */
/***********************************************************/
static u8 Motor_Enable(u8 Copy_u8MotrIndex, Register Copy_u8MotorDir);

#endif /* DCMOTOR_H_ */
