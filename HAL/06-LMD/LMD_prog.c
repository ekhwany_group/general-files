/*
 * LMD_prog.c
 *
 *  Created on: Feb 10, 2019
 *      Author: EsSss
 */



/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 11 Feb 2019									*/
/* VERSION : V1.0										*/
/* LAYER   : HAL 										*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to Implement The LMD APIs 			*/
/********************************************************/
/* INCLUDING THE LIBRARIES NEEDED   */
#include "STD_TYPES.h"
#include "BIT_CALC.h"
#include "SYSTICK_interface.h"
/* INCLUDING THE LOWER LAYER NEEDED	*/
#include "DIO_interface.h"
/* INCLUDING THE OWNER FILES        */
#include "LMD_interface.h"
#include "LMD_private.h"
#include "LMD_config.h"






/*************************************************************/
/* Description: This Function Implemented To Initialize      */
/*   			The LDM Component By Deactivating ALl Cols	 */
/* INPUTS: -	No Inputs									 */
/* OUTPUTS: The Error STATE									 */
/*************************************************************/
void LMD_VidInit(void){
	u8 Locale_RowsNumber=0;
	for(Locale_RowsNumber=INIT; Locale_RowsNumber<LMD_NUMBER_OF_ROWS; Locale_RowsNumber++)
	{
		/* Deactivate Of All Columns*/
		DIO_u8SetPinValue(LMD_Au8ActivPins[Locale_RowsNumber],DIO_u8_HIGH);
	}
}


/*************************************************************/
/* Description: This Function Implemented To Set The Define  */
/*   			The interface File Parameters 				 */
/* INPUTS: - Desired Image 									 */
/* 		   - Color	 										 */
/* OUTPUTS: The Error STATE									 */
/*************************************************************/
u8 LMD_u8Display(u8 *Copy_u8Image,u8 Copy_Color)
{
	/* LOCAL VARIABLE DEFINITION */
	u8 Locale_u8_StateError=ERROR_OK;
	u8 Locale_RowsNumber;
	/* Error State Validation */
	if(	(Copy_u8Image == NULL)
			||
	  (	(Copy_Color != RED) && (Copy_Color != GREEN) )	)
	{ Locale_u8_StateError = ERROR_NOK ; }

	/* Case Valid Inputs Parsed To the API */
	else {
		switch(Copy_Color)
		{
			case RED:	if ( LMD_ACTIVIVATION_TYPE == LMD_ACTIVE_LOW){
							for(Locale_RowsNumber=INIT; Locale_RowsNumber<LMD_NUMBER_OF_ROWS; Locale_RowsNumber++)
								{
									/* Activate The Column */
									DIO_u8SetPinValue(LMD_Au8ActivPins[Locale_RowsNumber],DIO_u8_LOW);
									u8SetDataPins(Locale_RowsNumber,Copy_u8Image,RED);
									DIO_u8SetPinValue(LMD_Au8ActivPins[Locale_RowsNumber],DIO_u8_HIGH);
								}
							}
						else if ( LMD_ACTIVIVATION_TYPE == LMD_ACTIVE_HIGH){
							for(Locale_RowsNumber=INIT; Locale_RowsNumber<LMD_NUMBER_OF_ROWS; Locale_RowsNumber++)
								{
									DIO_u8SetPinValue(LMD_Au8ActivPins[Locale_RowsNumber],DIO_u8_HIGH);


									/* M7taga Tet3adl be El complement Bta3 El Values Dy */
									u8SetDataPins(Locale_RowsNumber,Copy_u8Image,RED);
									/**********************************************/
									DIO_u8SetPinValue(LMD_Au8ActivPins[Locale_RowsNumber],DIO_u8_LOW);

								}
							}
						else{	/* DO NOTHING */					}
						break;

			case GREEN: if ( LMD_ACTIVIVATION_TYPE == LMD_ACTIVE_LOW){
							for(Locale_RowsNumber=INIT; Locale_RowsNumber<LMD_NUMBER_OF_ROWS; Locale_RowsNumber++)
								{
									DIO_u8SetPinValue(LMD_Au8ActivPins[Locale_RowsNumber],DIO_u8_LOW);
									u8SetDataPins(Locale_RowsNumber,Copy_u8Image,GREEN);
									DIO_u8SetPinValue(LMD_Au8ActivPins[Locale_RowsNumber],DIO_u8_LOW);
								}
							}
						else if ( LMD_ACTIVIVATION_TYPE == LMD_ACTIVE_HIGH){
								for(Locale_RowsNumber=INIT; Locale_RowsNumber<LMD_NUMBER_OF_ROWS; Locale_RowsNumber++)
								{
									DIO_u8SetPinValue(LMD_Au8ActivPins[Locale_RowsNumber],DIO_u8_HIGH);

									/* M7taga Tet3adl be El complement Bta3 El Values Dy */
									u8SetDataPins(Locale_RowsNumber,Copy_u8Image,RED);
									/**********************************************/
									DIO_u8SetPinValue(LMD_Au8ActivPins[Locale_RowsNumber],DIO_u8_LOW);
								}
							}
						else{	/* DO NOTHING */					}
						break;
			}
		}
	/* Return Error State */
	return Locale_u8_StateError;
}


/* This Private Function Implemented To set The LMD Pins */
/* By Corresponding Data 								 */
u8	u8SetDataPins(u8 Copy_u8RowsNumber, u8 *u8Image, u8 Copy_u8Color){

	switch (Copy_u8Color){
		case	RED: 	/* Display VAlues On data Pins */
						DIO_u8SetPinValue(LCD_U8_RED_ROW0,((Register)(u8Image[Copy_u8RowsNumber])).BitAccess.Bit0 );
						DIO_u8SetPinValue(LCD_U8_RED_ROW1,((Register)(u8Image[Copy_u8RowsNumber])).BitAccess.Bit1 );
						DIO_u8SetPinValue(LCD_U8_RED_ROW2,((Register)(u8Image[Copy_u8RowsNumber])).BitAccess.Bit2 );
						DIO_u8SetPinValue(LCD_U8_RED_ROW3,((Register)(u8Image[Copy_u8RowsNumber])).BitAccess.Bit3 );
						DIO_u8SetPinValue(LCD_U8_RED_ROW4,((Register)(u8Image[Copy_u8RowsNumber])).BitAccess.Bit4 );
						DIO_u8SetPinValue(LCD_U8_RED_ROW5,((Register)(u8Image[Copy_u8RowsNumber])).BitAccess.Bit5 );
						DIO_u8SetPinValue(LCD_U8_RED_ROW6,((Register)(u8Image[Copy_u8RowsNumber])).BitAccess.Bit6 );
						DIO_u8SetPinValue(LCD_U8_RED_ROW7,((Register)(u8Image[Copy_u8RowsNumber])).BitAccess.Bit7 );
						SysTick_Wait10us(200);
						break;
		case GREEN: 	/* Display VAlues On data Pins */
						DIO_u8SetPinValue(LCD_U8_GREEN_ROW0,((Register)(u8Image[Copy_u8RowsNumber])).BitAccess.Bit0 );
						DIO_u8SetPinValue(LCD_U8_GREEN_ROW1,((Register)(u8Image[Copy_u8RowsNumber])).BitAccess.Bit1 );
						DIO_u8SetPinValue(LCD_U8_GREEN_ROW2,((Register)(u8Image[Copy_u8RowsNumber])).BitAccess.Bit2 );
						DIO_u8SetPinValue(LCD_U8_GREEN_ROW3,((Register)(u8Image[Copy_u8RowsNumber])).BitAccess.Bit3 );
						DIO_u8SetPinValue(LCD_U8_GREEN_ROW4,((Register)(u8Image[Copy_u8RowsNumber])).BitAccess.Bit4 );
						DIO_u8SetPinValue(LCD_U8_GREEN_ROW5,((Register)(u8Image[Copy_u8RowsNumber])).BitAccess.Bit5 );
						DIO_u8SetPinValue(LCD_U8_GREEN_ROW6,((Register)(u8Image[Copy_u8RowsNumber])).BitAccess.Bit6 );
						DIO_u8SetPinValue(LCD_U8_GREEN_ROW7,((Register)(u8Image[Copy_u8RowsNumber])).BitAccess.Bit7 );
						SysTick_Wait10us(200);
						break;
	}
return 0;
}

