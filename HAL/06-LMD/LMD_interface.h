/*
 * LMD_interface.h
 *
 *  Created on: Feb 10, 2019
 *      Author: EsSss
 */

#ifndef LMD_INTERFACE_H_
#define LMD_INTERFACE_H_


/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 11 Feb 2019									*/
/* VERSION : V1.0										*/
/* LAYER   : HAL 										*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to Set The INTERFACES 				*/
/* 	Of LMD Component			 						*/
/********************************************************/

/****************************/
/* Type Of LMD 				*/
/* Range: -Active Rows		*/
/* 		  -Active Columns	*/
/****************************/
#define LMD_ACTIVE_ROWS			(u8)0
#define LMD_ACTIVE_COLs			(u8)1

/****************************/
/* Active Type 				*/
/* Range: -Active Low		*/
/* 		  -Active High		*/
/****************************/
#define LMD_ACTIVE_LOW			(u8)0
#define LMD_ACTIVE_HIGH			(u8)1

/****************************/
/* LMD COLORS				*/
/* Range: -RED				*/
/* 		  -GREEN			*/
/****************************/
#define RED 					(u8)0
#define GREEN					(u8)1


/*************************************************************/
/* Description: This Function Implemented To Initialize      */
/*   			The LDM Component By Deactivating ALl Cols	 */
/* INPUTS: -	No Inputs									 */
/* OUTPUTS: The Error STATE									 */
/*************************************************************/
void LMD_VidInit(void);

/*************************************************************/
/* Description: This Function Implemented To Set The Define  */
/*   			The interface File Parameters 				 */
/* INPUTS: - Desired Image 									 */
/* 		   - Color	 										 */
/* OUTPUTS: The Error STATE									 */
/*************************************************************/
u8 LMD_u8Display(u8 *Copy_u8Image,u8 Copy_Color);




#endif /* LMD_INTERFACE_H_ */
