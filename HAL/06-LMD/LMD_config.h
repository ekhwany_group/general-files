/*
 * LMD_config.h
 *
 *  Created on: Feb 10, 2019
 *      Author: EsSss
 */

#ifndef LMD_CONFIG_H_
#define LMD_CONFIG_H_



/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 11 Feb 2019									*/
/* VERSION : V1.0										*/
/* LAYER   : HAL 										*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to Set The Configure  				*/
/* 	The LMD Component with Desired Configuration		*/
/********************************************************/




/****************************/
/* Type Of LMD 				*/
/* Range: -Active Rows		*/
/* 		  -Active Columns	*/
/****************************/
#define		LMD_TYPE			LMD_ACTIVE_ROWS


/****************************/
/* Active Type 				*/
/* Range: -Active Low		*/
/* 		  -Active High		*/
/****************************/
#define LMD_ACTIVIVATION_TYPE	LMD_ACTIVE_LOW



/****************************/
/* The Number Of LMD Rows 	*/
/****************************/
#define LMD_NUMBER_OF_ROWS	(u8)8


/*****************************/
/* Activation Of  Columns    */
/*****************************/
u8 LMD_Au8ActivPins[]={   DIO_PIN0,
					  	  DIO_PIN1,
						  DIO_PIN2,
						  DIO_PIN3,
						  DIO_PIN4,
						  DIO_PIN5,
						  DIO_PIN6,
						  DIO_PIN7	};
/******************************/
/* Data/Display PINS 		  */
/******************************/
/* RED PINS */
#define LCD_U8_RED_ROW0          DIO_PIN28
#define LCD_U8_RED_ROW1          DIO_PIN29
#define LCD_U8_RED_ROW2          DIO_PIN30
#define LCD_U8_RED_ROW3          DIO_PIN31
#define LCD_U8_RED_ROW4          DIO_PIN8
#define LCD_U8_RED_ROW5          DIO_PIN9
#define LCD_U8_RED_ROW6          DIO_PIN10
#define LCD_U8_RED_ROW7          DIO_PIN11
/* GREEN PINS */
#define LCD_U8_GREEN_ROW0        DIO_PIN24
#define LCD_U8_GREEN_ROW1        DIO_PIN25
#define LCD_U8_GREEN_ROW2        DIO_PIN26
#define LCD_U8_GREEN_ROW3        DIO_PIN27
#define LCD_U8_GREEN_ROW4        DIO_PIN28
#define LCD_U8_GREEN_ROW5        DIO_PIN29
#define LCD_U8_GREEN_ROW6        DIO_PIN30
#define LCD_U8_GREEN_ROW7        DIO_PIN31

#endif /* LMD_CONFIG_H_ */


