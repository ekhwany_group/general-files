/*
 * KPD_config.h
 *
 *  Created on: Feb 16, 2019
 *      Author: EsSss
 */

#ifndef KPD_CONFIG_H_
#define KPD_CONFIG_H_

/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 16 JAN 2019									*/
/* VERSION : V1.0										*/
/* COMPONENT : KeyPad									*/
/********************************************************/
/* DESCRIPTION: 										*/
/* 	This file is used to Define the Configuration 		*/
/* 	Of KeyPad Component.								*/
/********************************************************/

/* Description:												*/
/* Its An Array of the KeyPad Connected PINs.				*/
/************************************************************/
#define 	KPD_CONNECTED_PINS			{	{DIO_PIN0,       \
											 DIO_PIN1,		 \
											 DIO_PIN2,		 \
											 DIO_PIN3},		 \
											{DIO_PIN4,		 \
										     DIO_PIN5,		 \
											 DIO_PIN6,		 \
											 DIO_PIN7}		 \
										}




/* Description:												*/
/* Its An Array of the KeyPad Type of PINs.					*/
/************************************************************/
#define 	KPD_CONNECTION_TYPE			{							 \
											DIO_PIN_DIR_INPUT_PULL_UP,		 \
										    DIO_PIN_DIR_INPUT_PULL_UP,		 \
											DIO_PIN_DIR_INPUT_PULL_UP,		 \
											DIO_PIN_DIR_INPUT_PULL_UP		 \
										}

/* Description:												*/
/* The Number Of the KeyPad Columns/						*/
/************************************************************/
#define 	KPD_COLUMNS_NUM		(u8)4

/* Description:												*/
/* The Number Of the KeyPad Row.							*/
/************************************************************/
#define 	KPD_ROWS_NUM		(u8)4



#endif /* KPD_CONFIG_H_ */
