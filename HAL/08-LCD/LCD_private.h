/*
 * LCD_private.h
 *
 *  Created on: Mar 4, 2019
 *      Author: EsSss
 */

#ifndef LCD_PRIVATE_H_
#define LCD_PRIVATE_H_

/******************************************************/
/* API: LCD_u8WriteData 							  */
/* Description : Write Data On The LCD				  */
/******************************************************/
/* Inputs : Data To Display 						  */
/* Outputs: Error State								  */
/******************************************************/
static void LCD_u8WriteData(u8 Copy_u8Data);
/******************************************************/
/* API: LCD_u8WriteCommand 							  */
/* Description : Sending commands To LCD Controller   */
/******************************************************/
/* Inputs : Desired Command							  */
/* Outputs: Error State								  */
/******************************************************/
static void LCD_u8WriteCommand(u8 Copy_u8Command);


static void PrivWrite(Register Data);

#endif /* LCD_PRIVATE_H_ */
