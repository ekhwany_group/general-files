/*
 * LCD_interface.h
 *
 *  Created on: Mar 4, 2019
 *      Author: EsSss
 */

#ifndef LCD_INTERFACE_H_
#define LCD_INTERFACE_H_

/* Register Selector Pin Modes */
#define LCD_RS_COMMAND	(u8)0
#define LCD_RS_DATA		(u8)1

/* R/W Pin Modes */
#define LCD_RW_WRITE	(u8)0
#define LCD_RW_READ		(u8)1

/* Values Of The Enable PIN */
#define LCD_E_HIGH		(u8)1
#define LCD_E_LOW		(u8)0

/* Maximum Vlue Can be Written On the LCD Controller */
#define LCD_MAX_COMMAND_DATA	(255)

/******************************************************/
/* API: LCD_Initialization 							  */
/* Description : Setup The LCD Module By Sending Some */
/* 				 Fixed Commands. 					  */
/******************************************************/
/* Inputs : No Inputs 								  */
/* Outputs: No Outputs 								  */
/******************************************************/
void LCD_Initialization(void);

u8 LCD_voidWriteStr(u8 *Copy_u8Data, u8 copy_u8XPos, u8 Copy_u8YPos);

#endif /* LCD_INTERFACE_H_ */
