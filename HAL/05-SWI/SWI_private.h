/*
 * SWI_private.h
 *
 *  Created on: Feb 7, 2019
 *      Author: EsSss
 */

#ifndef SWI_PRIVATE_H_
#define SWI_PRIVATE_H_


/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 31 JAN 2019									*/
/* VERSION : V1.0										*/
/********************************************************/
/* DESCRIPTION: 										*/
/* 	This file is used to Define the Interfaces 			*/
/* 	Of The software Component							*/
/********************************************************/



/* Types Of Switches */
#define PULL_UP 			(u8)1
#define PULL_DOWN 			(u8)0

#define SWI_U8_INIT			(u8)0

#define SWI_u8State_HIGH	(u8)1
#define SWI_u8State_LOW		(u8)0



#endif /* SWI_PRIVATE_H_ */
