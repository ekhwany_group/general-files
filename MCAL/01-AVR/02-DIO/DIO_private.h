/*
 * DIO_private.h
 *
 *  Created on: Jan 31, 2019
 *      Author: EsSss
 */


/******************** PRIVATE FILE ***********************/
/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 31 JAN 2019									*/
/* VERSION : V1.0										*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to Store the private parameters 	*/
/*of the DIO Component .								*/
/********************************************************/

#ifndef DIO_PRIVATE_H_
#define DIO_PRIVATE_H_

/* The Number Of The PORT PINS */
#define NUMBER_PINS_IN_PORT		8







/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to configure the software Component	*/
/*by Intiallizing each bin wither OUTPUT or INPUT		*/
/********************************************************/


/* DATA DIRECTION REGISTER ADDRESSES */
#define DIO_DDRA_Register	((Register*) 0x3A)->ByteAccess
#define DIO_DDRB_Register 	((Register*) 0x37)->ByteAccess
#define DIO_DDRC_Register 	((Register*) 0x34)->ByteAccess
#define DIO_DDRD_Register 	((Register*) 0x31)->ByteAccess


/* OUTPUT REGISTER ADDRESSES     */
#define DIO_PORTA_Register 	((Register*) 0x3B)->ByteAccess
#define DIO_PORTB_Register 	((Register*) 0x38)->ByteAccess
#define DIO_PORTC_Register 	((Register*) 0x35)->ByteAccess
#define DIO_PORTD_Register 	((Register*) 0x32)->ByteAccess

/* INPUT REGISTER ADDRESSES     */
#define DIO_PINA_Register 	((Register*) 0x39)->ByteAccess
#define DIO_PINB_Register 	((Register*) 0x36)->ByteAccess
#define DIO_PINC_Register 	((Register*) 0x33)->ByteAccess
#define DIO_PIND_Register 	((Register*) 0x30)->ByteAccess




/* OUTPUT BIT ACCESSES ADDRESSING */
#define PORTA_PIN0 	((Register*) 0x3B)->BitAccess.Bit0
#define PORTA_PIN1 	((Register*) 0x3B)->BitAccess.Bit1
#define PORTA_PIN2 	((Register*) 0x3B)->BitAccess.Bit2
#define PORTA_PIN3 	((Register*) 0x3B)->BitAccess.Bit3
#define PORTA_PIN4 	((Register*) 0x3B)->BitAccess.Bit4
#define PORTA_PIN5 	((Register*) 0x3B)->BitAccess.Bit5
#define PORTA_PIN6 	((Register*) 0x3B)->BitAccess.Bit6
#define PORTA_PIN7 	((Register*) 0x3B)->BitAccess.Bit7

#define PORTB_PIN0 	((Register*) 0x38)->BitAccess.Bit0
#define PORTB_PIN1 	((Register*) 0x38)->BitAccess.Bit1
#define PORTB_PIN2 	((Register*) 0x38)->BitAccess.Bit2
#define PORTB_PIN3 	((Register*) 0x38)->BitAccess.Bit3
#define PORTB_PIN4 	((Register*) 0x38)->BitAccess.Bit4
#define PORTB_PIN5 	((Register*) 0x38)->BitAccess.Bit5
#define PORTB_PIN6 	((Register*) 0x38)->BitAccess.Bit6
#define PORTB_PIN7 	((Register*) 0x38)->BitAccess.Bit7


#define PORTC_PIN0 	((Register*) 0x35)->BitAccess.Bit0
#define PORTC_PIN1 	((Register*) 0x35)->BitAccess.Bit1
#define PORTC_PIN2 	((Register*) 0x35)->BitAccess.Bit2
#define PORTC_PIN3 	((Register*) 0x35)->BitAccess.Bit3
#define PORTC_PIN4 	((Register*) 0x35)->BitAccess.Bit4
#define PORTC_PIN5 	((Register*) 0x35)->BitAccess.Bit5
#define PORTC_PIN6 	((Register*) 0x35)->BitAccess.Bit6
#define PORTC_PIN7 	((Register*) 0x35)->BitAccess.Bit6


#define PORTD_PIN0 	((Register*) 0x32)->BitAccess.Bit0
#define PORTD_PIN1 	((Register*) 0x32)->BitAccess.Bit1
#define PORTD_PIN2 	((Register*) 0x32)->BitAccess.Bit2
#define PORTD_PIN3 	((Register*) 0x32)->BitAccess.Bit3
#define PORTD_PIN4 	((Register*) 0x32)->BitAccess.Bit4
#define PORTD_PIN5 	((Register*) 0x32)->BitAccess.Bit5
#define PORTD_PIN6 	((Register*) 0x32)->BitAccess.Bit6
#define PORTD_PIN7 	((Register*) 0x32)->BitAccess.Bit7







#define DIO_PIN_INIT_DIRECTION_INPUT   0
#define DIO_PIN_INIT_DIRECTION_OUTPUT  1
#define DIO_PIN_INIT_VALUE_LOW         0
#define DIO_PIN_INIT_VALUE_HIGH        1
#define DIO_U8_PIN_DEF            0

#endif /* DIO_PRIVATE_H_ */
