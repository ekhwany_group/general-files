/*
 * RCC_project.c
 *
 *  Created on: Feb 24, 2019
 *      Author: EsSss
 */





/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 3 MAR 2019									*/
/* VERSION : V1.0										*/
/* LAYER   : MCAL										*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to Implement The RCC APIS and build */
/* Component Functionality 								*/
/********************************************************/


#include "STD_TYPES.h"
#include "BIT_CALC.h"
#include "RCC_private.h"
#include "RCC_interface.h"
#include "RCC_config.h"


/* API: Initialization of The RCC Peripheral */
/* Description :   Define Clock Source 		 */
/* 				   HSI State 				 */
/* 				   HSE State 				 */
/* 				   PLL  State 				 */
/* 				   PLL  Multiplier 			 */
/*				   PLL  Source 				 */
/* 				   AHB  Prescaler			 */
/*				   ABB1 Prescaler 		   	 */
/* 				   ABB2 Prescaler 			 */
/*********************************************/
void RCC_VidInitialize(void){

	/* ************Initialize CR Register**************** */
		/* Enable HSI Source */
		ASSIGN_BIT(RCC->CR.RegisterAccess,RCC_CR_HSI_ON,BIT_HIGH);
		/* Enable HSE Source */
		ASSIGN_BIT(RCC->CR.RegisterAccess,RCC_CR_HSE_ON,BIT_HIGH);
		/* Enable PLL Source */
		ASSIGN_BIT(RCC->CR.RegisterAccess,RCC_CR_PLL_ON,BIT_HIGH);
		/* Enable CSS Source */
		ASSIGN_BIT(RCC->CR.RegisterAccess,RCC_CR_CSS,BIT_HIGH);

		/* ************Initialize CFGR Register**************** */

		/* Select The System Clock Source */
		ASSIGN_BIT(RCC->CFGR.RegisterAccess,RCC_CFGR_SW_BIT0,SW_VAL_BIT0);
		ASSIGN_BIT(RCC->CFGR.RegisterAccess,RCC_CFGR_SW_BIT1,SW_VAL_BIT1);
		/* Select The PLL Multiplier */
		ASSIGN_BIT(RCC->CFGR.RegisterAccess,RCC_CFGR_MUL_BIT0, PLLMUL_VAL_BIT0);
		ASSIGN_BIT(RCC->CFGR.RegisterAccess,RCC_CFGR_MUL_BIT1, PLLMUL_VAL_BIT1);
		ASSIGN_BIT(RCC->CFGR.RegisterAccess,RCC_CFGR_MUL_BIT2, PLLMUL_VAL_BIT2);
		ASSIGN_BIT(RCC->CFGR.RegisterAccess,RCC_CFGR_MUL_BIT3, PLLMUL_VAL_BIT3);
		/* Select The PLL Source */
		ASSIGN_BIT(RCC->CFGR.RegisterAccess,RCC_CFGR_PLLSRC_BIT,PLLSRC_VAL);
		/* Select The AHB Presacaler */
		ASSIGN_BIT(RCC->CFGR.RegisterAccess,RCC_CFGR_HPRE_BIT0,HPRE_VAL_BIT0);
		ASSIGN_BIT(RCC->CFGR.RegisterAccess,RCC_CFGR_HPRE_BIT1,HPRE_VAL_BIT1);
		ASSIGN_BIT(RCC->CFGR.RegisterAccess,RCC_CFGR_HPRE_BIT2,HPRE_VAL_BIT2);
		/* Select The APB1 Presacaler */
		ASSIGN_BIT(RCC->CFGR.RegisterAccess,RCC_CFGR_PPRE1_BIT0,PPRE1_VAL_BIT0);
		ASSIGN_BIT(RCC->CFGR.RegisterAccess,RCC_CFGR_PPRE1_BIT1,PPRE1_VAL_BIT1);
		ASSIGN_BIT(RCC->CFGR.RegisterAccess,RCC_CFGR_PPRE1_BIT2,PPRE1_VAL_BIT2);
		/* Select The APB2 Presacaler */
		ASSIGN_BIT(RCC->CFGR.RegisterAccess,RCC_CFGR_PPRE2_BIT0,PPRE2_VAL_BIT0);
		ASSIGN_BIT(RCC->CFGR.RegisterAccess,RCC_CFGR_PPRE2_BIT0,PPRE2_VAL_BIT0);
		ASSIGN_BIT(RCC->CFGR.RegisterAccess,RCC_CFGR_PPRE2_BIT0,PPRE2_VAL_BIT0);
}


/************************************************************/
/* API: Set Peripheral State 								*/
/* Description : Define The State Of specific peripheral	*/
/************************************************************/
u8 RCC_u8SetPeripheralState(u8 Copy_u8Pheripheral, u8 Copy_u8State){
	/* Local Variable Definitions */
	u8 Locale_ErrorState;

	/* Input  Validation */
	if(	(Copy_u8Pheripheral >= RCC_PERI_BITS_NUM) ||
			((Copy_u8State != RCC_PERIPHERAL_ENABLE) &&
			 (Copy_u8State != RCC_PERIPHERAL_DISABLE))){
		Locale_ErrorState = ERROR_NOK;
	}
	else {
		Locale_ErrorState = ERROR_OK;

			if(Copy_u8Pheripheral < RCC_AHBENR_LIMIT){
				ASSIGN_BIT(RCC->AHBENR.RegisterAccess,Copy_u8Pheripheral,Copy_u8State);
			}
			else if (Copy_u8Pheripheral < RCC_ABB2ENR_LIMIT){
				ASSIGN_BIT(RCC->ABB2ENR.RegisterAccess,(Copy_u8Pheripheral-RCC_AHBENR_LIMIT),Copy_u8State);
			}
			else {
				ASSIGN_BIT(RCC->ABB1ENR.RegisterAccess,(Copy_u8Pheripheral-RCC_ABB2ENR_LIMIT),Copy_u8State);
			}
	}
	return Locale_ErrorState;
}



