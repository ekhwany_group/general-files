/*
 * RCC_private.h
 *
 *  Created on: Feb 24, 2019
 *      Author: EsSss
 */

#ifndef RCC_PRIVATE_H_
#define RCC_PRIVATE_H_


/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 3 MAR 2019										*/
/* VERSION : V1.0										*/
/* LAYER   : MCAL										*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to configure the software Component	*/
/********************************************************/

/* Structure Of RCC Register and its Configuration Bits */
typedef struct {
	Register_32Bit CR		;
	Register_32Bit CFGR		;
	Register_32Bit CIR		;
	Register_32Bit APB2RSTR	;
	Register_32Bit APB1RSTR	;
	Register_32Bit AHBENR	;
	Register_32Bit ABB2ENR	;
	Register_32Bit ABB1ENR	;
	Register_32Bit BDCR		;
	Register_32Bit CSR		;
}RCC_Register;

/* Define The Base Address of RCC Register */
#define RCC		((RCC_Register *)0x40021000)


#endif /* RCC_PRIVATE_H_ */
