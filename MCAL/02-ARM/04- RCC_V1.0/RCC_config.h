/*
 * RCC_config.h
 *
 *  Created on: Feb 24, 2019
 *      Author: EsSss
 */

#ifndef RCC_CONFIG_H_
#define RCC_CONFIG_H_


/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 3 MAR 2019										*/
/* VERSION : V1.0										*/
/* LAYER   : MCAL										*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to configure the software Component	*/
/*by Initializing each bin wither OUTPUT or INPUT		*/
/********************************************************/

/********************************************************/
			/* CFGR Register Configuration */

/* Define The HSI As A system Clock */
#define SW_VAL_BIT0			(u8)0
#define SW_VAL_BIT1			(u8)0

/* AHB Presacler as divided by 2	*/
#define HPRE_VAL_BIT0		(u8)0
#define HPRE_VAL_BIT1		(u8)0
#define HPRE_VAL_BIT2		(u8)0
#define HPRE_VAL_BIT3		(u8)1

/* ABB1 Presacler as divided by 2	*/
#define PPRE1_VAL_BIT0		(u8)0
#define PPRE1_VAL_BIT1		(u8)0
#define PPRE1_VAL_BIT2		(u8)1

/* ABB2 Presacler as divided by 2	*/
#define PPRE2_VAL_BIT0		(u8)0
#define PPRE2_VAL_BIT1		(u8)0
#define PPRE2_VAL_BIT2		(u8)1

/* ADC Presacler as divided by 2	*/
#define ADCPRE_VAL_BIT0		(u8)0
#define ADCPRE_VAL_BIT1		(u8)0

/* Set The MUL Value to *2 */
#define PLLMUL_VAL_BIT0		(u8)0
#define PLLMUL_VAL_BIT1		(u8)0
#define PLLMUL_VAL_BIT2		(u8)0
#define PLLMUL_VAL_BIT3		(u8)0

/* Choose The HSI divided by 2 As PLL Source */
#define PLLSRC_VAL		(u8)0

/* Define The prescaler Of the USB as clock divided by 2 */
#define USBPRE_VAL		(u8)0

/* Choose The Microcontroller Clock Output as System Clock */
#define MCO_VAL_BIT0			(u8)0
#define MCO_VAL_BIT1			(u8)0
#define MCO_VAL_BIT2			(u8)1

/*****************************************************************/

#endif /* RCC_CONFIG_H_ */
