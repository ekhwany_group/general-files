/*
 * RCC_interface.h
 *
 *  Created on: 3 MAR 2019
 *      Author: EsSss
 */

#ifndef RCC_INTERFACE_H_
#define RCC_INTERFACE_H_


/********************************************************/
/* Author: ISLAM GMAL ABDEL NASSER						*/
/* DATE: 24 Feb 2019									*/
/* VERSION : V1.0										*/
/* LAYER   : MCAL										*/
/* Component : Reset And Clock Control 			 		*/
/********************************************************/
/* DESCRIPTION: 										*/
/*This file is used to Include the interface of the RCC	*/
/*Component and APIs									*/
/********************************************************/



/* API: Initialization of The RCC Peripheral */
/* Description :   Define Clock State 		 */
/* 				   HSI State 				 */
/* 				   HSE State 				 */
/* 				   PLL  State 				 */
/* 				   PLL  Multiplier 			 */
/*				   PLL  Source 				 */
/* 				   AHB  Prescaler			 */
/*				   ABB1 Prescaler 		   	 */
/* 				   ABB2 Prescaler 			 */
/*********************************************/
void RCC_VidInitialize(void);



/* API: Set Peripheral State 								*/
/* Description : Define The State Of specific peripheral	*/
/************************************************************/
u8 RCC_u8SetPeripheralState(u8 Copy_u8Pheripheral, u8 Copy_u8State);



/* Maximum Peripheral Bits Size 96 */
#define RCC_PERI_BITS_NUM	(u8)96
/* Enabled PERIPHERAL */
#define RCC_PERIPHERAL_ENABLE	(u8)1
/* Disabled PERIPHERAL */
#define RCC_PERIPHERAL_DISABLE	(u8)0
/* First Peripheral Register */
#define	RCC_AHBENR_LIMIT	(u8)32
/* Second Peripheral Register */
#define	RCC_ABB2ENR_LIMIT	(u8)64
/* Third Peripheral Register */
#define	RCC_ABB1ENR_LIMIT	(u8)95
/****************************************************/
/*AHB Peripheral Clock enable register (RCC_AHBENR) */
#define RCC_AHBNER_DMA1_ENCLK_BIT			 (u8)0
#define RCC_AHBNER_DMA2_ENCLK_BIT            (u8)1
#define RCC_AHBNER_SRAM_ENCLK_BIT            (u8)2
#define RCC_AHBNER_FLITF_ENCLK_BIT           (u8)4
#define RCC_AHBNER_CRC_ENCLK_BIT             (u8)6
#define RCC_AHBNER_OTGFS_ENCLK_BIT           (u8)12
#define RCC_AHBNER_ETHKMAC_ENCLK_BIT         (u8)14
#define RCC_AHBNER_ETHMACTX_ENCLK_BIT        (u8)15
#define RCC_AHBNER_ETHMACRX_ENCLK_BIT        (u8)16
/*APB2 peripheral clock enable register (RCC_APB2ENR)*/
#define RCC_APB2ENR_AFIO_ENCLK_BIT			 (u8)32
#define RCC_APB2ENR_IOPA_ENCLK_BIT           (u8)34
#define RCC_APB2ENR_IOPB_ENCLK_BIT           (u8)35
#define RCC_APB2ENR_IOPC_ENCLK_BIT           (u8)36
#define RCC_APB2ENR_IOPD_ENCLK_BIT           (u8)37
#define RCC_APB2ENR_IOPE_ENCLK_BIT           (u8)38
#define RCC_APB2ENR_ADC1_ENCLK_BIT           (u8)41
#define RCC_APB2ENR_ADC2_ENCLK_BIT           (u8)42
#define RCC_APB2ENR_TIM1_ENCLK_BIT           (u8)43
#define RCC_APB2ENR_SPI1_ENCLK_BIT           (u8)44
#define RCC_APB2ENR_USART1_ENCLK_BIT         (u8)45
/*APB1 peripheral clock enable register (RCC_APB1ENR)*/
#define RCC_APB1ENR_TIM2_ENCLK_BIT			 (u8)64
#define RCC_APB1ENR_TIM3_ENCLK_BIT           (u8)65
#define RCC_APB1ENR_TIM4_ENCLK_BIT           (u8)66
#define RCC_APB1ENR_TIM5_ENCLK_BIT           (u8)67
#define RCC_APB1ENR_TIM6_ENCLK_BIT           (u8)68
#define RCC_APB1ENR_TIM7_ENCLK_BIT           (u8)69
#define RCC_APB1ENR_WWDG_ENCLK_BIT           (u8)75
#define RCC_APB1ENR_SPI2_ENCLK_BIT           (u8)78
#define RCC_APB1ENR_SPI3_ENCLK_BIT           (u8)79
#define RCC_APB1ENR_USART2_ENCLK_BIT         (u8)81
#define RCC_APB1ENR_USART3_ENCLK_BIT         (u8)82
#define RCC_APB1ENR_USART4_ENCLK_BIT         (u8)83
#define RCC_APB1ENR_USART5_ENCLK_BIT         (u8)84
#define RCC_APB1ENR_I2C1_ENCLK_BIT           (u8)85
#define RCC_APB1ENR_I2C2_ENCLK_BIT           (u8)86
#define RCC_APB1ENR_CAN1_ENCLK_BIT           (u8)89
#define RCC_APB1ENR_CAN2_ENCLK_BIT           (u8)90
#define RCC_APB1ENR_BKP_ENCLK_BIT            (u8)91
#define RCC_APB1ENR_PWR_ENCLK_BIT            (u8)92
#define RCC_APB1ENR_DAC_ENCLK_BIT            (u8)93
/*******************************************************/

/* BIT HIGH VALUE */
#define BIT_HIGH 	(u8)1
/* BIT LOW VALUE */
#define BIT_LOW  	(u8)0





/* Enable BIT of The High SPeed Internal Clock Source */
#define RCC_CR_HSI_ON			(u8)0
/* Ready BIT of The High SPeed Internal Clock Source */
#define RCC_CR_HSI_RDY			(u8)1
/* Enable BIT of The High SPeed External Clock Source */
#define RCC_CR_HSE_ON			(u8)16
/* Ready BIT of The High SPeed External Clock Source */
#define RCC_CR_HSE_RDY			(u8)17
/* Enable BIT OF The High SPeed External PyBass Clock Source */
#define RCC_CR_HSE_PYB			(u8)18
/* Enable BIT OF The Phase Locked Loop Clock Source */
#define RCC_CR_PLL_ON			(u8)24
/* Ready BIT of The Phase Locked Loop Clock Source  */
#define RCC_CR_PLL_RDY			(u8)25
/* Enable BIT OF The Check Security System 			*/
#define RCC_CR_CSS				(u8)19

#define RCC_CFGR_SW_BIT0		(u8)0
#define RCC_CFGR_SW_BIT1		(u8)1

#define RCC_CFGR_SWS_BIT0		(u8)2
#define RCC_CFGR_SWS_BIT1		(u8)3
/* AHB Prescaler BITS */
#define RCC_CFGR_HPRE_BIT0		(u8)4
#define RCC_CFGR_HPRE_BIT1		(u8)5
#define RCC_CFGR_HPRE_BIT2		(u8)6
#define RCC_CFGR_HPRE_BIT3		(u8)7

/* APB1 Prescaler BITs */
#define RCC_CFGR_PPRE1_BIT0		(u8)8
#define RCC_CFGR_PPRE1_BIT1		(u8)9
#define RCC_CFGR_PPRE1_BIT2		(u8)10

/* APB2 Prescaler BITs */
#define RCC_CFGR_PPRE2_BIT0		(u8)11
#define RCC_CFGR_PPRE2_BIT1		(u8)12
#define RCC_CFGR_PPRE2_BIT2		(u8)13

/* ADC Prescaler BITs */
#define RCC_CFGR_ADCPRE_BIT0	(u8)14
#define RCC_CFGR_ADCPRE_BIT1	(u8)15

/* PLL Source BIT	*/
#define RCC_CFGR_PLLSRC_BIT				(u8)16

/* PLL MUL BITs */
#define RCC_CFGR_MUL_BIT0				(u8)18
#define RCC_CFGR_MUL_BIT1				(u8)19
#define RCC_CFGR_MUL_BIT2				(u8)20
#define RCC_CFGR_MUL_BIT3				(u8)21

/* USB Prescaler BITs		*/
#define RCC_CFGR_USB				(u8)22

/* Microcontroller Clock Output	*/
#define RCC_CFGR_MCO_BIT0	(u8)24
#define RCC_CFGR_MCO_BIT1	(u8)25
#define RCC_CFGR_MCO_BIT2	(u8)26



#define RCC_CFGR_
#endif /* RCC_INTERFACE_H_ */
