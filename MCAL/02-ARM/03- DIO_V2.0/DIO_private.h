/*
 * ARM_DIO_private.h
 *
 *  Created on: Feb 22, 2019
 *      Author: Ahmed Tarek
 */
/*************************************ARM_DIO_private.h*********************************************/
/* Author  : Ahmed Tarek                                                      					     */
/* Date    :Feb 27, 2019                                                        					 */
/* Version : v01                                                                  					 */
/*****************************************************************************************************/
/*Description												                                 		  */
/*-----------												                               			  */
/*ARM_DIO_private.h is a file from DIO Deriver include the Addresses of registers      			      */
/******************************************************************************************************/

#ifndef DIO_PRIVATE_H_
#define DIO_PRIVATE_H_


#define DIO_MAX_DIR_VALUE	  (u8)0b0000
#define DIO_MIN_DIR_VALUE	  (u8)0b1111


#define DIO_u8_PIN_NO_4       (u8)4
#define DIO_u8_PIN_NO_5       (u8)5
#define DIO_u8_PIN_NO_8       (u8)8
#define DIO_u8_PIN_NO_12      (u8)12
#define DIO_u8_PIN_NO_16      (u8)16
#define DIO_u8_PIN_NO_20      (u8)20
#define DIO_u8_PIN_NO_24      (u8)24
#define DIO_u8_PIN_NO_28      (u8)28
#define DIO_u8_PIN_NO_32      (u8)32
#define DIO_u8_PIN_NO_35      (u8)35
#define DIO_u8_MAX_PIN_NO     (u8)37

#define DIO_u8_PORT_SIZE     (u8)16





typedef struct
{
	Register_32Bit	CRL;
	Register_32Bit	CRH;
	Register_32Bit	IDR;
	Register_32Bit	ODR;
	Register_32Bit	BSRR;
	Register_32Bit	BRR;
	Register_32Bit	LCR;


}GPIO;

/*define macro act as pointer to struct */
#define DIO_GPIOA	((GPIO*)0x40010800)
#define DIO_GPIOB	((GPIO*)0x40010C00)
#define DIO_GPIOC	((GPIO*)0x40011000)
#define DIO_GPIOD	((GPIO*)0x40011400)

/*to access them */
/*WordAccess : DIO_GPIOA -> CRL.WordAccess */
/*BitAccess : DIO_GPIOA -> CRL.BitdAccess.Bit# */
/*use this technique with any peripheral has contiguous registers */

static u8 u8SetPin(u8 Copy_u8Port , u8 Copy_u8Pin);
static u8 u8ResetPin(u8 Copy_u8Port , u8 Copy_u8Pin);



#define	PRIV_CRL_LIMIT	8
#define PRIV_CRH_LIMIT	15



#endif /* ARM_DIO_PRIVATE_H_ */
