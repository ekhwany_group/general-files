'''**************** Python **************
*     Name    : Isalm Gamal             *
*     Session : 1 , 2                   *
*     Target  : Python Sysntax          *
*     Date    : 29th Dec 2018           *
*                                       *
'''***********************************'''


http://developers.google.com/edu/python/

******************************************************
Python is a popular programming + Scripting Language *
******************************************************

 - Its a case sensitive Language
 - its a very sensitive language to indintation . 
 - Variables has no data types 
 - logical and physical lines are considered 
 - strings in python declared between ('')
 - string + string : is a concatination in python
 - Functions un python defined without any return type or input argu type 
        - def Fun_Name(input_argu) : 
 - Modules are imported only in the global space and its runned sequentially.
 - Every python script have to contain a boilerplate: wich is the the entry point of your script.
      biolerplate : if __name__ == '__main__' :
                        main()
  

  
********************************************************************************
***************** Session 2 ****************************
********************************************************************************

/-------------------------------------------------------
/ Lists : List Is considered as an array but it can contains non homogenous data.
/ SYNTAX:      List_Name = [val1,val2,val3,....]
/ -------------------------------------------------------
         List index starts form 0 
         List accessing : list_name[1] = val1 
         creating another pointer for the same array : list_name2 = list_name1  
         copying the all list of a list to another list :  list_name2 = list_name1[:] 
         List elements can be changed any time without loops: List_name = [new1,new2,..]
         Lists can be Multidimensional

         ****** LIST METHODs *****
          list_name.append(val)         : Used To Append value at the end of the list
          list_name.insert(index,val)   : Used To Insert a value inside the list
          list_name.extend(List_name2)  : Used To Extend list1 by values of list2
          list_name.index(val)          : Used To search for the index of any value
          list_name.remove(val)         : Used To remove a value from the list
          list-name.sort()              : Used To Sort the list
          list-name.reverse()            : Used To reverse the arrangement of the list  
          sorted(list)                  : Return the sorted version of the list without changing the list itself
          list_name.pop()               : Used to pop a value from the list.


/-------------------------------------------------------/
/ FOR LOOP :
/  SYNTAX  : For i in List :
/              some_code .....
/-------------------------------------------------------/      
            For i in range(any range): To control the range of the loop.  
            range(start,end,step)    : Used To manage the behaviour of the loop. 
            in                       : used alone to search for something in a list. 
            For i in list[::2] :
              some_code ......       : Used to control the step of iterrator in a list
            Comprehensive list       :  Is a List that created from specific expresion 
                              list    = [1,2,3]
                              ComList = [n*n for n in list]   #[1,4,9]
            Comprehensive list Attributes :
              -  <,>,= value    : to creat a list if the element in the another list < or > or = specific VAL                  
              -  if condition   : to creat a list if the element in the another list achieve a specific Condition 
 
-------------------------------------------------------
 While Loop :
                SYNTAX : WHILE i CONDITION 
                            some_Code .....
                            i = i + step 
-------------------------------------------------------
     



  
********************************************************************************
      ***************** Session 3 ****************************
******************************************************************************** 

/-------------------------------------------------------
/ SORTING : Used to Sort specific list with more than on way of sorting.     
/ -------------------------------------------------------
list.sort()              : Changing the list itself to sorted one.
list.reverse()           : Reverse the sorting of length from end to start.
sorted(list)             : Return a sorted version without changing list itself.
sorted(list,key=len,reverse=True)     : Used to return a sorted list based on len reversly.
sorted(list,key=feature) : Used to return a sorted list based on any specified feature.

/-------------------------------------------------------
/ SEQUENCE TYPES : IS A SEQUENCE OF ELEMENTS WITH SOME FEATURES
/ -------------------------------------------------------
  1- TUPLE :: Is CONSIDERED AS AN EMUTABLE LIST or An STRUCT.
      A = (1,2,'hi') : this is a list(tuble) contains 1,2,hi.
      A = (1,)  : Is on value TUBLE.
      Tuple : its values Can not be changed.
      (X,Y,Z)=(1,2,3) : Tuble Assignment. Hence, Z=3 , Y=2 , Z=3 but the 2 tubles must be same LENGTH.
      List_Tuble = [(1,2,3),(4,5,6),(7,8,9)] : is A list contains 3 tubles.
      TUBLE : can be used to return more than value from a function.
      TUBLE : Accepts the concatination Operation (1,2,3)+(3,4,5)=(1,2,3,4,5,6).
 
 /-------------------------------------------------------
/ MAPPING TYPES : IS A MAPPING FOR KEY TO ITS VALUES.
/ -------------------------------------------------------
  DICTIONARIES : USED TO CREAT SOME SORT OF DATA BASE 
      DECT_NAME={'KEY1' : 'VALUE', 'KEY2':'VALUE2'} : ITS Dictionary of 2 Elements.
      DECT KEYs  : Can be only integers, strings, tubles.
      DECT VALUES : Can be any Type. 
      Search on Dect : can be applied using only keys -> DECt_NAME['key']
      Search on list of values for key : can be applied used expression -> DECt_NAME['key'][0]
      DICTIONARy APPEND: can be Appended -> DECT.append 
                        Dict['NewKey']='New'
                        Dict.update({'NewKey':'NewValue'})
      DICTIONARy Concatination  : Can be used.
      DICTIONARY contains Some OF FUNCTIONS :
          DICT.get('key') : Return The valaue of the key.
          DICT.keys  ()   : Return a list of KEYs.
          DICT.values()   : Return a list of values.
          DICT.items ()   : Return a list of tubles.
      LOOPs on DICt itearates over its KEYS by defualt.
      DEL func : Can be used with DICT to Remove any key and its value 
                  -> del dict['key']
                  
                  
                    
********************************************************************************
      ***************** Session 4 ****************************
******************************************************************************** 

/-------------------------------------------------------
/ Files Operations : All Operations depend on Files.
/ -------------------------------------------------------
/-------------------------------------------------------/
/ Openning Files :
/  SYNTAX  : File_Handeler = open('FileName.text','Mode')
      or      with open('FileName.text','Mode') as  File_Handeler: to open temporarily
      or      with open('FileName.text','Mode+b') as  File_Handeler: to read the file as binary 
/ Closin Files :
/  SYNTAX  : File_Handeler.close()
/-------------------------------------------------------/  
Files Parameters:
    'r' : For Read Only Mode.
    'w' : For Write only mode, If file not exists it will be created,
          If it exists it will be overwritted.
    'a' : For appending only, If file not exists it will be created.
    'r+': For Read + write Mode, the file has to be already created.
    'rU': Universal read whatever the String encoding,
          it reads with reference to all types of string encoding.

/-------------------------------------------------------/
/ Reading Files :
/  SYNTAX  : File_Handler.read(index) : Index is optional.
/-------------------------------------------------------/  
Files Functions:
  handler.close()      : To close file.
  handler.seek()       : Return the File index pointer to specific location.
  handler.readline()   : Return single line frol the file and increment file index by line.
  handler.readlines()  :Return a list of the file lines.
  handler.splitlines() : USed to split line without file ends(\n,\r,..). 
                          It can receive any type of delimetor.
  handler.rstrip(removables) : Starting from the end, deleminate all characte parsed to rstrib function-> (removables).
                  [start:end] : slicing. 
                  [index] : accessing by index.
                  Ex. handler.realines()[0][:-1] : it returns the first element of alist of line Except the final character.
  handler.write('string') : Used to write a string in the file, to save the updated value
                            the file have to be closed. 
  handler.tell() : Return the curren byte of index pointer in the file.
  handler.seek(offset,From_What): 
                From_What : {0,1,2} , 0-> means start from 0, 1-> means start from current POsition,
                                      2-> means start from the end of file.
                Offset : it means the step to move the index pinter(-or+).
  handler.closed : return true if the file closed , false if the file is opened.
  with open('FileName.text','Mode') as  File_Handeler: This utility open the file temporarily 
                                                    for some operation then is will be auto closed.
                                                    
               
                    
********************************************************************************
      ***************** Session 6 ****************************
******************************************************************************** 

/-------------------------------------------------------
/ GUI Design in the Python : creation for GUI using PySide Lib  
/ -------------------------------------------------------